package advisory

import (
	"strings"
)

// Package contains the information needed to resolve a package on the server side.
type Package struct {
	Type string `json:"type"`
	Name string `json:"name"`
}

// Slug returns the package slug/path.
func (p Package) Slug() string {
	return p.Type + "/" + p.Name
}

// UnmarshalYAML decodes a package slug.
func (p *Package) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var slug string
	if err := unmarshal(&slug); err != nil {
		return err
	}

	parts := strings.SplitN(slug, "/", 2)
	if len(parts) > 1 {
		p.Name = parts[1]
	}
	p.Type = parts[0]

	return nil
}
